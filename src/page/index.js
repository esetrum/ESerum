import LoginPage from './Login';
import RegisterPage from './Register';
import AlertPage from './Alert';
import ScanIoTPage from './ScanIoT';
import PairingRFIDPage from './PairingRFID';

export {LoginPage, RegisterPage, AlertPage, ScanIoTPage, PairingRFIDPage};
